<?php
    $title       = "Distribuidora de Móveis Hospitalares em Minas Gerais";
    $description = "Fale com um de nossos especialistas para garantir o mais rápido possível, os melhores móveis hospitalares da região. Estamos sempre disponíveis e aptos para te apresentarmos nossos serviços. ";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    <?php // include "includes/modal-orcamento-qsmi.php"; ?>

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php // echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p><br />Você que está buscando pela melhor distribuidora de móveis hospitalares em Minas Gerais, garantimos que não há empresa melhor que a movmed, para atender as suas necessidades. Estamos expandindo a cada dia a mais, sendo uma das maiores empresas desse segmento na região. Nós estamos há longos anos atuando como distribuidora de móveis hospitalares em Minas Gerais, contando com profissionais que possuem grandes experiências para exercerem suas funções dentro de nossa empresa. Nossos móveis hospitalares podem ser adaptáveis em diversos ambientes, seja em clínicas, laboratórios ou até mesmo em sua própria casa. Nós fornecemos biombo duplo e triplo, braçadeira para injeção e diversos outros móveis em nossa distribuidora de móveis hospitalares em Minas Gerais, portanto não deixe de garantir o melhor que se adapte a você. Fabricamos sempre nossos móveis hospitalares com os melhores materiais para que os mesmos possam ser utilizados para qualquer exame que é preciso independente do momento e lugar. Todos os móveis de nossa distribuidora de móveis hospitalares em Minas Gerais levarão grande conforto e segurança aqueles que irão utilizar dos mesmos, pois nos preocupamos com os nossos clientes em todos os quesitos. Todos os representantes da nossa distribuidora de móveis hospitalares em Minas Gerais são especialistas nesse segmento para acompanharem e efetuarem desde o serviço de atendimento, fabricação até mesmo a entrega. Nós fornecemos poltrona hospitalar para coleta de sangue, bancos e mochos e diversos outros móveis hospitalares. Ou seja, a nossa distribuidora de móveis hospitalares em Minas Gerais é capaz de atender a todos os seus pedidos. Lembrando que os memos são adaptáveis a qualquer tipo de ambiente, sendo que para qualquer que for o desejado, você conseguirá aprimorá-lo. Ao obter nossos produtos, garantimos a você que estará levando mais conforto e confiança aos seus pacientes ou a quem desejar. Todos os nossos móveis hospitalares têm grande funcionalidade em sua clínica ou laboratório, pois além de facilitar a realização de certos exames, levam mais conforto e segurança a quem irá utilizar dos mesmos, fazendo com que criem cada vez mais fidelidade a você e ao seu trabalho. Nossa distribuidora de móveis hospitalares em Minas Gerais fornece em toda essa região e até mesmo em outros lugares do Brasil, portanto de onde estiver, você conseguirá aprimorar nossos móveis ao seu ambiente. Desde seu primeiro contato conosco, você verá que temos os melhores recursos e serviços para te oferecer, fazendo com que sempre que precisar, você recorra a nós. Não adie cada vez mais a oportunidade de aprimorar o ambiente de sua clínica ou laboratório, levando cada vez mais conforto aos seus pacientes, com os nossos produtos de qualidade, que você só encontra em nossa distribuidora de móveis hospitalares em Minas Gerais. </p>
<h2>Mais detalhes sobre nossos serviços como distribuidora de móveis hospitalares em Minas Gerais</h2>
<p><br />No site da nossa distribuidora de móveis hospitalares em Minas Gerais você poderá ver fotos de nossos móveis, conhecendo várias opções dos mesmos. Mas caso prefira, nossos meios de contatos estão sempre disponíveis para que você possa falar conosco. Ao necessitar de um ou mais móveis hospitalares, consulte a movmed, distribuidora de móveis hospitalares em Minas Gerais para que nossos especialistas te auxiliam na hora de sua escolha, fazendo também um orçamento personalizado a você. Ao falar com um de nossos profissionais, ou navegar em nosso site, você verá móveis hospitalares para todos os tipos de exame e o que mais se adapte ao que você deseja no momento, mantendo sempre o conforto que queremos passar e principalmente segurança. Os profissionais da nossa distribuidora de móveis hospitalares em Minas Gerais, além de já possuírem longos anos de estudos e experiências, aprimoram seus conhecimentos a cada dia mais, para que possamos sempre manter uma extrema qualidade em nossos produtos, nos adaptando sempre a nova tecnologia que é usada nos materiais de fabricação dos nossos móveis hospitalares. Após absorver todas essas informações sobre nossos serviços e produtos como distribuidora de móveis hospitalares em Minas Gerais, garanta o quanto antes nossos móveis, fazendo o seu orçamento conosco, até mesmo sem compromisso. Afirmamos que nossos móveis hospitalares servem para todo tipo de exame e ambiente, portanto, garanta já o seu. Além de que você só obterá vantagens a sua clínica ou laboratório ao adquirir os móveis da nossa distribuidora de móveis hospitalares em Minas Gerais. Agarre a oportunidade de ter nossos móveis hospitalares de alta qualidade, mantendo um baixo custo a você, modernizando sempre seu ambiente. Fale com a nossa equipe o quanto antes. Trabalhamos duro dia após dia, para que possamos nos tornar cada vez mais referência e exemplo como distribuidora de móveis hospitalares em Minas Gerais e também no Brasil todo.</p>
<h2>A melhor opção para distribuidora de móveis hospitalares em Minas Gerais</h2>
<p><br />Todos os especialistas da nossa distribuidora de móveis hospitalares em Minas Gerais, estão sempre disponíveis para que você possa entrar em contato conosco através de nossos meios de contatos, como nosso e-mail em nosso site, específico para que você possa tirar suas devidas dúvidas sobre nossos trabalhos ou serviços. Mas caso prefira, possuímos nossas redes sociais para que você possa acompanhar nossos produtos de perto e até mesmo números telefônicos para falar diretamente com um de nossos profissionais. Entre em contato com a nossa distribuidora de móveis hospitalares em Minas Gerais e veja todos os móveis que temos disponíveis a você. Nós fazemos e acompanhamos todos os processos de serviço até que nossos móveis estejam em suas mãos. Nosso objetivo sempre é fazermos com que não só atendemos as suas expectativas, mas superarmos, para que você se surpreenda conosco em todos os quesitos. E para isso, a nossa distribuidora de móveis hospitalares em Minas Gerais se for necessário, até refaz o processo de fabricação para entregarmos nossos produtos da forma que imaginamos. O conjunto de nossos profissionais fazem sempre um excelente trabalho, para que você tenha incríveis experiências conosco, em todos os quesitos. Como distribuidora de móveis hospitalares em Minas Gerais, nós cuidamos de todos os serviços para você, para que tenha os nossos móveis hospitalares com a mais alta durabilidade.</p>
                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>