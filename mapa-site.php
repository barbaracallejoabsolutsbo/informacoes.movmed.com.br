4<?php

    $title       = "Mapa do Site";
    $description = "Em nosso mapa do site você encontra todas as páginas do site, confira nossas categorias e informações de nossos produtos e serviços."; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php"; 
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "mapa-site"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <div class="text-right">
                <?php echo $padrao->breadcrumb(array($title)); ?>
            </div>
            <h1><?php echo $h1; ?></h1>
            <div class="sitemap">
                <?php include "includes/estrutura-site.php"; ?>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>