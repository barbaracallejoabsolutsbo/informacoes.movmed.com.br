<?php

    $title       = "404";
    $description = "Página não encontrada."; // Manter entre 130 a 160 caracteres
    $h1          = "404 - Página não Encontrada";
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php"; 
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "mapa-site"
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array($title)); ?>
            <h1><?php echo $h1; ?></h1>
            <p>Por favor, selecione uma página abaixo.</p>
            <div class="sitemap">
                <?php include "includes/estrutura-site.php"; ?>
            </div>
        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $padrao->compressJS(array(
        
    )); ?>
    
</body>
</html>