<?php
    $title       = "Móveis Hospitalares em Minas Gerais";
    $description = "Todos os nossos meios de contatos estão disponíveis, para que você possa falar com qualquer um de nossos atendentes. ";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    <?php // include "includes/modal-orcamento-qsmi.php"; ?>

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php // echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    
<p>Nossos profissionais são aptos para acompanharam todo o processo de fabricação de nossos móveis hospitalares em Minas Gerais, pois se especializaram durante anos para tal. Estamos há longos anos oferencendo sempre os melhores móveis hospitalares em Minas Gerais, para que mais pessoas possam ter o conforto que merecem num momento que para muitos, pode ser considerado vulnerável. Nos preocupamos com o que nossos clientes irão sentir ao utilizar nossos móveis e até mesmo ao ter qualquer tipo de contato conosco e para isso, nos colocamos primeiramente na posição de cliente para que possamos entregar os melhores serviços em todas as formas.</p>
<p>Nossa empresa está localizada em Londrina, mas caso necessite de nossos móveis hospitalares em Minas Gerais, ou demais outros Estados do País, não se preocupe, pois atendemos a nível Brasil. Afirmamos que todos os nossos clientes recebem o melhor e mais qualificado atendimento, pois nossos profissionais se qualificam a cada dia mais para isso. Todos os nossos meios de contatos e profissionais estão disponíveis a qualquer momento para que você possa tirar suas dúvidas sobre nossos móveis hospitalares em Minas Gerais ou até mesmo sobre nossos serviços.</p>
<h2>Mais detalhes sobre nossos móveis hospitalares em Minas Gerais</h2>
<p>Além de nossos móveis hospitalares em Minas Gerais mudarem a estética de seu ambiente; de primeira, os seus pacientes de seu laboratório ou clínica, verão que foram ao lugar certo para realizarem determinados exames, pois terão toda a segurança e conforto que esperavam. Motivos os quais fazem os mesmos buscaram por sua clínica ou laboratório quando precisarem. E você só conseguirá obter os melhores móveis hospitalares em Minas Gerais, com a movmed. Nos procure o quanto antes para obter nossos móveis, pois temos o objetivo de fazer com que cada um que for utilizar dos mesmos, sintam o conforto que merecem. Estamos sempre estudando e nos atualizando a esse mercado e as novas tecnologias que são impostas aos nossos materiais utilizadas na fabricação de nossos móveis hospitalares em Minas Gerais, para levarmos mais praticidade aos nossos clientes e os mesmos verem em nossa prática, que realmente somos exemplo de móveis hospitalares em Minas Gerais e no Brasil todo. Entrar em contato o quanto antes com um de nossos atendentes para garantir seus móveis hospitalares em Minas Gerais.</p>
<p>Garantindo nossos móveis hospitalares em Minas Gerais você terá um diferencial em sua clínica ou laboratório para que seus pacientes queiram voltar quando necessitarem dos seus serviços prestados. Queremos sempre fazer com que nossos clientes tenho as melhores experiências conosco em todos os quesitos portanto desde seu primeiro contato com a nossa empresa você terá um atendimento qualificado e personalizado para que você receba o atendimento que merece e também, para que todas as suas necessidades sejam correspondidas. Até mesmo para que você se surpreenda com a excelência que realizamos todos os serviços prestados na movmed.</p>
<p>Além das vantagens mostradas no texto que nossos móveis hospitalares em Minas Gerais darão a sua clínica ou laboratório, lhe mostraremos mais algumas para que você possa ter mais confiança em nossos trabalhos:</p>
<p>Conforto<br />•Segurança<br />•Funcionalidade<br />•Qualidade<br />•Higiene e manutenção<br />•Estética</p>
<h2>A melhor opção para móveis hospitalares em Minas Gerais</h2>
<p>Aplicamos todos os nossos conhecimentos absorvidos durante todos os nossos anos nesse mercado, na fabricação de nossos produtos, para que nos tornemos a cada dia mais referência para quem buscar por móveis hospitalares em Minas Gerais. Temos o objetivo de sempre levar mais praticidade aos nossos clientes e para isso, estamos sempre atendendo as necessidades dos mesmos com os recursos usados nos procedimentos de execução de nossos móveis. As tecnologia dos materiais utilizados na fabricacao de nossos móveis hospitalares em Minas Gerais são sempre aplicadas nos mesmos, pois queremos fazer com que cada um que for utilizar nossos móveis, sintam o máximo de conforto possível. Nossos produtos são de extrema qualidade e o melhor de tudo é que os preços dos mesmo são altamente acessíveis, para que você possa receber os melhores móveis hospitalares em minas gerias, sem possuir nenhum tipo de prejuízo financeiro.</p>
<p>Um dos nossos maiores objetivos é podermos corresponder as necessidades de nossos clientes e para isso, nossos profissionais se dispõem totalmente ao que você deseja ao consultar nossa empresa. Pois queremos fazer com que nossos clientes tenham expedicionárias incríveis e se sintam confortáveis em todas as fases de nosso atendimento e não somente ao garantir nossos móveis hospitalares em Minas Gerais. Fazemos o possível para que nossos clientes se surpreendam de forma Positiva com os nossos trabalhos desde seus primeiro contato com a movmed.</p>
<p>Priorizamos ser transparentes Com os nossos clientes portanto conheça mais um pouco sobre nosso vendo o que mais valorizamos dentro da nossa empresa:</p>
<p><br />• Respeito e ética.<br /> <br />• Qualidade e Pontualidade.<br /> <br />• Cordialidade e Fidelidade.<br /> <br />• Comprometindo, Transparência e Profissionalismo</p>
<p><br />Não espera mais tempo para ter os nossos móveis hospitalares em Minas Gerais, que são os melhores do Brasil. E nossos profissionais estão disponíveis a qualquer momento em que você nos solicitar para obter nossos móveis ou até mesmo para ver o processo do seu pedido. Analisamos a fabricação de nossos móveis com extrema atenção, para que você tenha nossos móveis hospitalares em Minas Gerais, da maneira que imaginou.</p>
<p>Visualize mais detalhadamente em nosso site sobre cada um de nossos móveis hospitalares. Você pode garantir dos mais variados em seu pedido e na quantidade que quiser. Temos várias opções de móveis hospitalares, como:</p>
<p>Braçadeira para injeção<br /> <br />• Suporte p/saco hamper<br /> <br />• Suporte de soro<br /> <br />• Luminária flexível<br /> <br />• Biombo duplo e triplo<br /> <br />• Escada clínica 02 degraus</p>
<p>Temos o orgulho de levarmos nossos móveis hospitalares em Minas Gerais e a tantos lugares do nosso país. Pois no dedicamos 100% aos nossos trabalhos, para que nossos clientes sintam o conforto que merecem no momento de realizarem certos exames. Te aguardamos ansiosamente através de nossos meios de contratos, ou até mesmo com a sua vinda em nossa empresa, para que você tenha detalhes únicos sobre os nossos móveis hospitalares em Minas Gerais. Não se esqueça de ficar atento as nossas redes sociais para ver informações exclusivas sobre nossos produtos. Conte sempre com os nosso serviços. </p>
                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>