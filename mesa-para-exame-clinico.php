<?php
    $title       = "Mesa Para Exame Clinico";
    $description = "Se você possuir mais alguma dúvida que não pode ser esclarecida em nosso texto, lembre-se que nosso e-mail para tirar dúvidas está sempre disponível e todas as redes sociais também, junto com os nossos números de telefone.";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    <?php // include "includes/modal-orcamento-qsmi.php"; ?>

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php // echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    
<p>Queremos sempre fazer com que nossos clientes recebam não só as nossas mesa para exame clínico, mas como todos os nossos produtos com extrema pontualidade. Nos tornamos a cada dia mais referência à entrega de diversos móveis hospitalares, pois nossos anos atribuindo grandes experiência fizeram com que melhorássemos cada vez mais a nossa qualidade.<br />Você pode obter qualquer um de nossos produtos de onde estiver e caso queira conhece-los de perto, nossa empresa está localizada em Londrina; mas não se preocupe pois enviamos nossas mesa para exame clínico e demais produtos a todo o Brasil. Nossos profissionais só exercem suas funções pois possuem anos de estudos no ramo de móveis hospitalares e grandes experiências na execução de mesa para exame clínico, portanto fazem todo o processo de fabricação com total atenção e excelência. A qualquer momento em que você nos procurar, você verá que nosso atendimento é único em todos os aspectos; fatores que nos levam a ter cada vez mais excelência na qualidade de nossas mesa para exame clínico e demais outros móveis hospitalares. Todos os representantes da movmed estão em prontidão quando você precisar nos apresentar quaisquer dúvidas sore nossas mesa para exame clínico através de nosso e-mail específico para isso. Ou você pode entrar em contato conosco através de nossos telefones e até mesmo redes sociais.</p>
<p>Mais detalhes sobre mesa para exame clínico <br />As mesa para exame clínico são necessárias para diversos tipos de atendimentos e exames, sendo para hospitais, laboratórios ou clínicas. Sabemos que realizar exames é muito necessário, até mesmo os de rotina e garantir uma mesa para exame de qualidade, leva o conforto que seus pacientes buscam para esse momento que possui alta tensão para alguns.<br />Portanto, possuir nossas mesa para exame clínico em sua clínica ou laboratório é extremamente importante, pois seus pacientes irão se sentir confortáveis num momento necessário. Até mesmo para aqueles que têm algum tipo de receio na hora de fazer certos exames até mesmo por estar acostumado a não se sentir confortável nesse determinado momento. E garantindo nossas mesa para exame clínico, faz com que através dessa experiência, te procurem quando necessitarem de seus serviços. Todavia, nos procure o quanto antes para garantir nossas mesa para exame clínico e demais outros móveis hospitalares.<br />Obter nossas mesa para exame clínico é um grande diferencial à sua empresa, pois além dos mesmos perceberem a importância que é dada ao conforto deles, aprimorarão a estética do ambiente desejado, já causando a primeira boa impressão. E com a movmed você terá os melhores móveis hospitalares você garantirá os melhores materiais para a execução dos nossos móveis hospitalares. Não aguarde mais tempo para nos procurar, pois garantimos que todas as suas expectativas sobre os nossos trabalhos serão superadas, pois nossos profissionais entregam excelentes trabalhos com alta qualidade há longos anos.</p>
<p>Antes que você garanta nossas mesa para exame clínico, lhe mostraremos mais algumas vantagens para obtê-las:<br />• Próprio para exames, repouso e massagem;</p>
<p>• Fácil manuseio;</p>
<p>• Design moderno;</p>
<p>• Conforto em sua utilização</p>
<p>A melhor opção para mesa para exame clínico</p>
<p>Todos os nossos conhecimentos absorvidos em nossos anos de atuação, fizeram com que nos tornássemos os melhores no ramo de mesa para exame clínico, não só em teoria, como na prática; para que então sejamos altamente procurados por aqueles que precisam de qualquer móvel hospitalar. A todo momento estudamos e pesquisando a nova tecnologia de nossos materiais, para levarmos mais conforto e funcionalidade ao utilizarem nossas mesa para exame clínico, pois nosso maior objetivo é fazer com que todos os que utilizam nossos produtos, tenham experiências únicas. Priorizamos fazer com que os preços de nossos serviços e produtos sejam de fácil acesso e para isso, apresentamos diversas formas de pagamento para que você não tenha que se preocupar financeiramente ao nos consultar.</p>
<p>Visamos sempre fazer com que sejamos a única opção de nossos clientes, quando precisarem de mesa para exame clínico para a realização de determinados exames, ou demais móveis hospitalares, portanto entregamos sempre nossos melhores serviços para que cada experiência conosco seja única. Desde seu primeiro contato conosco, nossos especialistas estão aptos para sanarem qualquer tipo de dúvida que você possuir, pois nossos profissionais são altamente qualificados.</p>
<p>Mantemos sempre a transparência com todos os nossos clientes e para que você nos conheça um pouco mais, fazermos a questão de te mostrar alguns de nossos valores:</p>
<p>• Respeito e ética.<br /> <br />• Qualidade e Pontualidade.</p>
<p>• Cordialidade e Fidelidade.</p>
<p>• Comprometindo, Transparência e Profissionalismo</p>
<p><br />Obtenha o quanto antes nossas mesa para exame clínico para que você conheça nossos produtos em mãos e não só na teoria. Desde o começo da produção de todos os nossos móveis e até mesmo ao transporte, temos o máximo de cautela para que alcancemos seus objetivos para com os nossos serviços. Se você possuir mais alguma dúvida que não pode ser esclarecida em nosso texto, lembre-se que nosso e-mail para tirar dúvidas está sempre disponível e todas as redes sociais também, junto com os nossos números de telefone; para que você possa falar conosco no momento em que precisar e de onde estiver. É de extremo agrado para nós, podermos levar nossas mesa para exame clínico a diversas clínicas e laboratórios do Brasil. Não perca mais tempo adiando a sua garantia das nossas mesa para exame clínico; ao ter o mínimo contato conosco você verá que nossos profissionais fornecem um atendimento jamais visto. Estamos à disposição até mesmo para fazermos o seu orçamento para as nossas mesa de exame clínico; não deixe de falar conosco para te entregar esse e demais outros produtos com a maior qualidade e pontualidade do mercado.</p>
<p><br />Será um prazer podermos te apresentar nossos serviços e até mesmo criarmos uma parceria, para que quando você precisar de mesa para exame clínico ou demais outros móveis hospitalares, você já saiba que somos o único lugar que pode oferecer o atendimento que você tanto quer. Atendemos a todo o Brasil para que possamos levar o conforto de nossos móveis pra que mais pessoas conheçam nossos produtos de alta qualidade. Conte sempre com a movmed!</p>
                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>