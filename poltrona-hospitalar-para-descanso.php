<?php
    $title       = "Poltrona Hospitalar Para Descanso";
    $description = "Saiba que a nossa empresa está de braços e portas abertas para te ajudar a qualquer momento com os nossos serviços e produtos como a poltrona hospitalar para descanso. Não hesite em nos chamar quando necessitar de poltrona hospitalar para descanso. Estamo";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    <?php // include "includes/modal-orcamento-qsmi.php"; ?>

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php // echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p><br />A poltrona hospitalar para descanso é adaptável a qualquer ambiente hospitalar, sejam eles clínicas ou até mesmo laboratórios, pois nela os pacientes obtêm o descanso e conforto que precisam em determinado momento. E para que você possa entregar aos seus pacientes o que eles desejam, é de extrema importância contar com a nossa empresa que é reconhecida pela qualidade não só na nossa poltrona hospitalar para descanso, mas como para os outros móveis hospitalares que fornecemos em nossa empresa. Nossa poltrona hospitalar para descanso oferece suporte para os braços e regulagem de encosto dos pés, para priorizarmos sua comodidade. E por conta do suporte para os braços, a poltrona hospitalar para descanso pode ser usada até mesmo quando for preciso a administração de medicações e coleta de sangue.</p>
<p>Por uma exigência técnica, a poltrona hospitalar para descanso necessita que seja feita com os materiais de mail alta qualidade, para que não ocorra nenhum tipo de acidente com o paciente, colocando sua saúde física em grande risco. E um dos grandes diferenciais da movmed são o conjunto de qualidade de nossos produtos, não só na produção mas até mesmo que determinado produto esteja em suas mãos. Todos os nossos profissionais são especializados e experientes em suas respectivas áreas e funções para que possamos corresponder todas as suas expectativas para com a nossa empresa. Atuamos há longos anos atuando nesse mercado onde estamos cada vez mais nos tornando referência nesse segmento.</p>
<h2>Mais detalhes sobre poltrona hospitalar para descanso</h2>
<p><br />A nossa poltrona hospitalar para descanso será extremamente útil a você possui uma clínica ou laboratório, pois seus pacientes encontrarão o conforto que tanto desejam e também para que não sintam nenhum receio na hora em que será necessário o seu uso. E somente conosco você conseguirá encontrar uma poltrona hospitalar para descanso de qualidade e que não trará nenhum prejuízo a você aqueles que a utilizarão. Priorizamos sempre usar dos materiais mais renomados e atualizados para que nossos clientes tenham a praticidade, conforto e segurança que esperam. Portanto, garanta o quanto antes a nossa poltrona hospitalar para descanso.</p>
<p>Todavia, possuir a nossa poltrona hospitalar para descanso é importante a você, pois possuir móveis hospitalares de qualidade em sua clínica ou laboratório irá fazer com que seus pacientes queiram voltar a realizar respectivos exames com você. Nos preocupamos com todos que terão algum tipo de contato conosco ou até mesmo nossos produtos, portanto desde a execução fazemos todos os processos com a máxima cautela para que todos os que utilizarem não só a nossa poltrona hospitalar para descanso, mas como todos nossos outros produtos, se surpreendam com o que vêm.<br />Lhe mostraremos à seguir mais algumas vantagens para garantir a sua poltrona hospitalar para descanso conosco:</p>
<p>• 4 posições de inclinação;</p>
<p>• Inclinação por engate rápido acionada por manopla;</p>
<p>• Apoio de braço e de pernas;</p>
<p>• Inclinação simultânea de encosto, braços e apoio de pernas;</p>
<p>• Estofamento com espuma;</p>
<p>• Fácil limpeza;</p>
<p>• Capacidade para 150 kg;</p>
<h2>A melhor opção para poltrona hospitalar para descanso</h2>

<p>Nossos longos anos de experiência fornecendo hospitalar para descanso, nos ajudaram a aprimorarmos nossos conhecimentos e experiências para que colocássemos em prática e então nos tornamos cada vez mais referência no segmento de móveis hospitalares. Nos atualizamos e adaptamos as novas tecnologias a cada dia mais, para que nossos clientes tenham não só a nossa poltrona hospitalar para descanso, mas como todos os nossos produtos em primeira mão com a mais alta qualidade e praticidade, que são um dos fatores que mais priorizamos em nossa empresa. Fazemos a questão de te apresentarmos diversas opções de pagamento, juntamente com os nossos valores extremamente acessíveis para que ao atribuir nossos produtos à sua empresa, você não tenha que se preocupar financeiramente.</p>
<p>Desde sempre optamos oferecer sempre nosso melhor atendimento independente do objetivo de seu contato, para que quando você precisar recorrer a uma empresa que fornece poltrona hospitalar para descanso ou demais móveis hospitalares à sua clínica ou laboratório, você pense na movmed como sua primeira opção. Garantimos isso pois nossa prioridade é fazer com que todas as suas necessidades sejam correspondidas pela nossa empresa desde o seu primeiro contato conosco.</p>
<p><br />Nossos valores para com os nossos clientes são:</p>
<p>• Respeito e ética.</p>
<p>• Qualidade e Pontualidade.</p>
<p>• Cordialidade e Fidelidade.</p>
<p>• Comprometindo, Transparência e Profissionalismo<br />Consulte nossos profissionais o quanto antes para que você perceba em prática toda a nossa teoria, garanta o quanto antes a nossa poltrona hospitalar para descanso e perceba toda a qualidade e excelência de nossos produtos. O transporte de nossos móveis são feitos com extrema cautela para que você receba nossos produtos além do jeito que imaginou. Se você possuir mais algum tipo de dúvida sobre nossos serviços e produtos, temos um e-mail especificamente para você enviá-la a nós, ou se preferir nossos outros meios de contato como também redes sociais estão sempre disponíveis para que você possa entrar em contato conosco.</p>
<p>Navegue mais em nosso site para conhecer de uma forma mais aprofundada sobre nossos outros tipos de poltronas e cadeiras; sendo elas: <br />• Bancos e mochos</p>
<p>• Poltrona para coleta de sangue</p>
<p>• Poltrona hospitalar para descanso e acompanhante</p>
<p>• Cadeiras para coleta de sangue</p>
<p><br />É de extremo agrado para nós levarmos nossa poltrona hospitalar para descanso e todos os nossos produtos ao seu laboratório ou clínica. Garantimos que você se surpreenderá com os nossos atendimentos e produtos, pois desde seu primeiro contato fazemos a questão de que você tenha experiências únicas com a nossa empresa. Nosso maior diferencial é a nossa pontualidade de entrega, portanto fale conosco no momento em que você desejar para que em todos os sentidos não só atenderemos suas expectativas, como as superaremos. Nossos profissionais estão sempre disponíveis através de nossos meios de contato para atenderem a qualquer pedido seu; até mesmo para fazermos seu orçamento seu compromisso algum.</p>
<p><br />Saiba que a nossa empresa está de braços e portas abertas para te ajudar a qualquer momento com os nossos serviços e produtos como a poltrona hospitalar para descanso. Não hesite em nos chamar quando necessitar de poltrona hospitalar para descanso. Estamos disponíveis qualquer momento. </p>
                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>