<?php
    $title       = "Maca Ginecológica";
    $description = "Será um prazer apresentarmos nossa maca ginecológica a sua clínica ou laboratório. Conte sempre com os nossos especialistas, para te auxiliarem no que for necessário ";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    <?php // include "includes/modal-orcamento-qsmi.php"; ?>

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php // echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    
<p>Buscamos sempre fazer com que a nossa maca ginecológica seja entregue aos nossos clientes com toda a qualidade e pontualidade possível, o que infelizmente não é fácil de encontrar nesse mercado. Melhoramos nossos serviços a cada dia, para que possamos ser cada vez mais excelentes em todos os aspectos, nos tornando referência nesse ramo para todo o país.</p>
<p>Atendemos a todo o Brasil, para que mais pessoas possam sentir o máximo de conforto ao utilizar nossa maca ginecológica. Caso você prefira conhecer nossos produtos de perto, estamos localizados em Londrina. As portas de nossa empresa estão sempre abertas para que possamos receber você. Foram longos anos de atuação até nos tornamos os melhores fabricantes de maca ginecológica do Brasil. Onde nossos profissionais absorveram grandes conhecimentos e experiências, aplicando-os cada vez mais em cada fase do processo de fabricação de nossos produtos. Possuímos uma qualidade exclusiva em nossos móveis hospitalares por conta desses e demais outros fatores. Te atenderemos a qualquer momento, pois um de nossos maiores objetivos é fazermos o que for possível para que suas necessidades sejam correspondidas. Contudo te ajudaremos no que precisar e com o que pudermos, com os recursos que temos. E ao consultar nosso site você verá que temos um e-mail específico para que você possa nos mandar suas devidas dúvidas perante a nossa maca ginecológica. E também diversas redes sociais e números telefônicos para que você possa falar diretamente com um de nossos atendente.</p>
<h2>Mais detalhes sobre a nossa maca ginecológica</h2>
<p>A nossa maca ginecológica dará grandes benefícios a sua clínica ou laboratório, pois ao ser utilizada para certos exames, seus pacientes não sentirão qualquer tipo de desconforto.</p>
<p>Possuir uma maca ginecológica de qualidade é extremamente importante para sua empresa, pois através da experiência de seus pacientes com ela, mais fidelidade será criada, fazendo com que quando necessitarem de exames ginecológicos, sua clínica ou laboratório seja o primeiro lugar que possam recorrer. E também, para que além de possuir o necessário, que é a maca ginecológica, você também possa ter um diferencial, levando extremo conforto e segurança aos que forem utilizá-la. Portanto, garanta o quanto antes não só a sua maca ginecológica, como também diversos outros móveis hospitalares, com a melhor fabricante do Brasil.</p>
<p>Com a maca ginecológica da movmed além da confiança em seu trabalho seus pacientes terão a primeira boa impressão ao ver uma maca de extrema qualidade E também perceberam a sua preocupação com o bem estar deles em sua clínica ou laboratório. Por isso não deixe de falar conosco para garantir a sua marca ginecológica para que possamos superar as expectativas de seus pacientes e até mesmo as suas para conosco.</p>
<p>Para que você tenha total certeza antes de garantir nossa maca ginecológica, vamos enfatizar alguns de seus benefícios dentre tantos outros. Segue-os abaixo</p>
<p>* confortado <br />* Segurança <br />* Praticidade</p>
<p>A melhor opção para maca ginecológica</p>
<p>Somos cada vez mais procurados por quem precisa de uma maca ginecológica, pois sempre nos dedicamos ao nossos clientes, entregando todos os nossos conhecimentos adquiridos em experiências, em nossos produtos, consequentemente nos tornando também referência nesse ramo. Conforme a tecnologia dos materiais utilizados na fabricação da nossa marca ginecológico sou atualizados nos adaptamos as suas atualizações para que cada pessoa que A utilizarem da mesma tenham experiências incríveis e se surpreendo com sua praticidade. Queremos sempre levar mais conforto e menos complicações aos nossos clientes e para isso, possuímos diversas formas de pagamento e valores acessíveis para que nossos clientes não tenham demais preocupações.</p>
<p>Afirmamos que desde seu primeiro e mínimo contato com os profissionais da move mede você terá toda a atenção necessária para que juntos alcançamos o seu objetivo de adquirir a melhor maca ginecológica para o seu ambiente desejado, pois queremos levar o máximo de conforto em todos os quesitos pera nossos clientes. Estamos sempre disponíveis para te atender e te ajudarmos no que for necessário.</p>
<p>Para que nos conheça um pouco mais, lhe mostraremos nossos princípios para te atender:</p>
<p>• Respeito e ética.<br /> <br />• Qualidade e Pontualidade.<br /> <br />• Cordialidade e Fidelidade.<br /> <br />• Comprometindo, Transparência e Profissionalismo</p>
<p>Para que você possa ter nossa maca ginecológica em suas mãos para conhecer nossos produtos de perto, fale com nossos profissionais o quanto antes. Queremos superar suas expectativas em todos os quesitos e para isso, todo o processo de fabricação da nossa maca ginecológica são altamente analisados e se necessário até revisados, para que você receba nossos produtos do jeito que imaginou. Nosso e-mail para que você possa nos contar suas ideias sobre nossos produtos, tirar dúvidas ou até mesmo fazermos seu orçamento, está disponível a qualquer momento que você desejar e de onde estiver. Ou até mesmo nossos números de telefones e redes sociais para que você possa acompanhar nossos produtos de forma mais detalhada e com exclusividade.</p>
<p>Além da nossa maca ginecológica, possuímos diversos outros móveis hospitalares para o seu laboratório ou clínica. Em nosso site você conseguirá conhecer mais detalhadamente sobre cada um. Saiba quais são eles:</p>
<p>• Braçadeira para injeção<br /> <br />• Suporte p/saco hamper<br /> <br />• Suporte de soro<br /> <br />• Luminária flexível<br /> <br />• Biombo duplo e triplo<br /> <br />• Escada clínica 02 degraus<br /> <br />É uma imensa felicidade para nós levarmos nossa maca ginecológica para diversos lugares do Brasil. Pois sabemos também que mais pessoas tiveram um atendimento de qualidade desde o primeiro contato, até o último, que é um de nossos objetivos. Portanto, consulte-nos para usufruir de nossos serviços e ver a qualidade dos mesmos. Não adie cada vez mais para obter a melhor maca ginecológica. Sabemos da necessidades de rapidez que nossos clientes têm quando nos procuram, portanto visamos sempre entregar nossos produtos com a máxima pontualidade. Te esperamos ansiosamente para que possamos fazer até mesmo o seu orçamento para a nossa maca ginecológica. Estamos há longos anos nesse ramo e garantimos que você não terá arrependimentos ao entrar em contato conosco. Nossos profissionais são altamente qualificados e se adaptarão a qualquer tipo de necessidade, para que você tenha sempre o atendimento que merece. Conte com a melhor empresa na hora de escolher os seus móveis hospitalares. Conte com os servciços que disponibilizamos a você. </p>
                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>