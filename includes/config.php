<?php

    // Principais Dados do Cliente
    $nome_empresa = "MOVMED";
    $emailContato = "comercial@movmed.com.br";

    // Parâmetros de Unidade
    $unidades = array(
        1 => array(
            "nome" => "MOVMED",
            "rua" => "Rodovia Celso Garcia Cid",
            "bairro" => "Portal de Versalhes",
            "cidade" => "Londrina",
            "estado" => "Paraná",
            "uf" => "PR",
            "cep" => "86044-290",
            "latitude_longitude" => "", // Consultar no maps.google.com
            "ddd" => "43",
            "telefone" => "3373-9649",
            "whatsapp" => "98807-0776",
            "link_maps" => "" // Incorporar link do maps.google.com
        ),
        2 => array(
            "nome" => "",
            "rua" => "",
            "bairro" => "",
            "cidade" => "",
            "estado" => "",
            "uf" => "",
            "cep" => "",
            "ddd" => "",
            "telefone" => ""
        )
    );
    
    // Parâmetros para URL
    $padrao = new classPadrao(array(
        // URL local
        "http://localhost/informacoes.movmed.com.br/",
        // URL online
        "https://informacoes.movmed.com.br/"
    ));
    
    // Variáveis da head.php
    $url = $padrao->url;
    $canonical = $padrao->canonical;
	
    // Parâmetros para Formulário de Contato
    $smtp_contato            = ""; // Solicitar ao líder do dpto técnico, 177.85.98.119
    $email_remetente         = ""; // Criar no painel de hospedagem, admin@...
    $senha_remetente         = "c0B1S3vH5eCvAO";

    // Contato Genérico (para sites que não se hospedam os e-mails)
    // $smtp_contato            = "111.111.111.111";
    // $email_remetente         = "formulario@temporario-clientes.com.br";
    // $senha_remetente         = "4567FGHJK";

    // Recaptcha Google
    $captcha                 = false; // https://www.google.com/recaptcha/
    $captcha_key_client_side = "";
    $captcha_key_server_side = "";

    // CSS default
    $padrao->css_files_default = array(
        "default/reset",
        "default/grid-system",
        "default/main",
        "default/slicknav-menu",
        "_main-style"
    );
    
    // JS Default
    $padrao->js_files_default = array(
        "default/jquery-1.9.1.min",
        "default/modernizr",
        "default/jquery.slicknav.min",
        "jquery.padrao.main"
    );
        
    // Listas de Palavras Chave
    $palavras_chave = array(
        "Poltrona Hospitalar Para Coleta de Sangue",
"Poltrona Hospitalar Para Descanso",
"Poltrona Hospitalar Preço",
"Móveis Hospitalares em São Paulo",
"Móveis Hospitalares no Paraná",
"Móveis Hospitalares no Rio de Janeiro",
"Móveis Hospitalares em Minas Gerais",
"Móveis Hospitalares Preço",
"Mesas Para Exame Clinico",
"Suporte de Soro a Venda",
"Fábrica de Móveis Hospitalares em São Paulo",
"Fábrica de Móveis Hospitalares em Minas Gerais",
"Fábrica de Móveis Hospitalares no Paraná",
"Fábrica de Móveis Hospitalares no Rio de Janeiro",
"Distribuidora de Móveis Hospitalares",
"Distribuidora de Móveis Hospitalares em São Paulo",
"Distribuidora de Móveis Hospitalares em Minas Gerais",
"Venda de Maca Hospitalar",
"Venda de Maca Hospitalar em São Paulo",
"Venda de Maca Hospitalar em Minas Gerais",
"Venda de Maca Hospitalar no Rio de Janeiro",
"Venda de Maca Hospitalar no Paraná",
"Venda de Cadeira Hospitalar",
"Mesa Auxiliar Para Hospital",
"Mesa Para Exame Clinico",
"Poltrona Para Descanso de Acompanhante",
"Mesa Para Consultório Médico",
"Cadeira de Coleta de Sangue",
"Cadeira de Coleta em São Paulo",
"Cadeira de Coleta em no Rio de Janeiro",
"Cadeira de Repouso hospitalar",
"Mesa Ginecológica",
"Fábrica Cadeira Coleta",
"Cadeira de Coleta Preço",
"Mesa Ginecológica Preço",
"Fábrica de Mesa Ginecológica",
"Maca Ginecológica",
"Maca para Estética",
"Maca para Estética em São Paulo",
"Maca para Estética Preço"
    );
   
    $palavras_chave_com_descricao = array(
        "Item 1" => "Lorem ipsum dolor sit amet.",
        "Item 2" => "Laudem dissentiunt ut per.",
        "Item 3" => "Solum repudiare dissentiunt at qui.",
        "Item 4" => "His at nobis placerat.",
        "Item 5" => "Ei justo lucilius nominati vim."
    );
    
     /**
     * Submenu
     * 
     * $opcoes = array(
     * "id" => "",
     * "class" => "",
     * "limit" => 9999,
     * "random" => false
     * );
     * 
     * $padrao->subMenu($palavras_chave, $opcoes);
     * 
     */

    /**
     * Breadcrumb
     * 
     * -> Propriedades
     * 
     * Altera a url da Home no breadcrumb
     * $padrao->breadcrumb_url_home = "";
     * 
     * Altera o texto que antecede a Home
     * $padrao->breadcrumb_text_before_home = "";
     * 
     * Altera o texto da Home no breadcrumb
     * $padrao->breadcrumb_text_home = "Home";
     * 
     * Altera o divisor de níveis do breadcrumb
     * $padrao->breadcrumb_spacer = " » ";
     * 
     * -> Função
     * 
     * Cria o breadcrumb
     * $padrao->breadcrumb(array("Informações", $h1));
     * 
     */

    /**
     * Lista Thumbs
     * 
     * $opcoes = array(
     * "id" => "",
     * "class_div" => "col-md-3",
     * "class_section" => "",
     * "class_img" => "img-responsive",
     * "title_tag" => "h2",
     * "folder_img" => "imagens/thumbs/",
     * "extension" => "jpg",
     * "limit" => 9999,
     * "type" => 1,
     * "random" => false,
     * "text" => "",
     * "headline_text" => "Veja Mais"
     * );
     * 
     * $padrao->listaThumbs($palavras_chave, $opcoes);
     * 
     */
    
    /**
     * Funções Extras
     * 
     * $padrao->formatStringToURL();
     * Reescreve um texto em uma URL válida
     * 
     */